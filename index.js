var AWS = require("aws-sdk");

AWS.config.update({
  region: "us-east-1"
});

var docClient = new AWS.DynamoDB.DocumentClient();

var table = "books";
var authorTable = "authors"

// need for cross-site POST to work
const corsHeaders = {
    'Access-Control-Allow-Origin': '*',
    "Access-Control-Allow-Methods": "GET, PUT, POST, DELETE",
    'Access-Control-Allow-Headers': "*"
};

async function newBook(book) {
    book.isbn = parseInt(book.isbn);
    book.pages = parseInt(book.pages);
    let params = {
        TableName: table,
        Item: book
    };
    
      const awsRequest = docClient.put(params);
    return awsRequest.promise()
    .then(data => {
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    },err => {
        console.log("Uh-oh!");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}

async function deleteBook(book) {
    
    book.isbn = parseInt(book.isbn);
    let params = {
        "TableName": table,
        "Key" : {
            "isbn": book.isbn
        }
    };
    
     const awsRequest = docClient.delete(params);
    return awsRequest.promise()
    .then(data => {
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    },err => {
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}

async function getAllBooks(){
    let params = {
    TableName: table, 
    Select: "ALL_ATTRIBUTES"
  };
  const awsRequest = docClient.scan(params);
    return awsRequest.promise()
    .then(data => {
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    },err => {
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}


//Auhtors functinos
async function getAllAuthors(){
    let params = {
    TableName: authorTable, 
    Select: "ALL_ATTRIBUTES"
  };
  const awsRequest = docClient.scan(params);
    return awsRequest.promise()
    .then(data => {
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    },err => {
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}
function deleteAuthor(author){
    author.aid = parseInt(author.aid);
    let params = {
        "TableName": authorTable,
        "Key" : {
            "aid": author.aid
        }
    };
    
     const awsRequest = docClient.delete(params);
    return awsRequest.promise()
    .then(data => {
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    },err => {
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}
function newAuthor(author){
        author.aid = parseInt(author.aid);
    let params = {
        TableName: authorTable,
        Item: author
    };
    
    const awsRequest = docClient.put(params);
    return awsRequest.promise()
    .then(data => {
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    },err => {
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
    
}
exports.handler = async (event) => {
    // sample event properties:
    // event.path: '/default/library/books'
    // event.httpMethod: 'POST'
    // event.body: json encoded object

    console.log( event.path );
    console.log( event.httpMethod );
    let body = JSON.parse(event.body);
    console.log( body );
    
    if ( event.httpMethod == 'POST' && event.path == '/default/library/books') {
        return newBook(body);
    }
    
    if ( event.httpMethod == 'DELETE' && event.path == '/default/library/books') {
        return deleteBook(event.queryStringParameters);
    }
    
    if(event.httpMethod == 'GET' && event.path=="/default/library/books"){
       return getAllBooks();
    }
    
    if ( event.httpMethod == 'POST' && event.path == '/default/library/authors') {
        return newAuthor(body);
    }
    
    if ( event.httpMethod == 'DELETE' && event.path == '/default/library/authors') {
        return deleteAuthor(event.queryStringParameters);
    }
    
    if(event.httpMethod == 'GET' && event.path=="/default/library/authors"){
       return getAllAuthors();
    }
    
    
    
    const response = {
        statusCode: 200,
        headers: corsHeaders,
        body: JSON.stringify('Unrecognized request'),
    };
    return response;
};