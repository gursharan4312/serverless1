import React, { Component } from 'react';
import axios from 'axios';


const baseURL ="https://iniy5bobt6.execute-api.us-east-1.amazonaws.com/default/library";

class Authors extends Component {
  constructor(props){
    super(props);
    this.state={
      authors:[]
    }
    this.deleteAuthor = this.deleteAuthor.bind(this);
    this.refreshAuthorList = this.refreshAuthorList.bind(this);
  }
   refreshAuthorList(){
     axios.get(baseURL + "/authors")
        .then(response=>{
          this.setState({
            authors:response.data.Items
          })
        })
   }
   
   deleteAuthor(aid){
       axios.delete(baseURL+"/authors",{params:{aid}})
    .then(response=>{
          this.refreshAuthorList();
        })
     .catch(err=>console.log(err))
  }
  
  addAuthor(ev) {
    const inputAID = ev.target.querySelector('[id="aid"]');
    const aid = inputAID.value.trim();

    const inputFname = ev.target.querySelector('[id="fname"]');
    const fname = inputFname.value.trim();

    const inputLname = ev.target.querySelector('[id="lname"]');
    const lname = inputLname.value.trim();

    
      let newAuthor = {
      aid,
      fname,
      lname
    };
    
    axios.post( baseURL + "/authors", newAuthor)
    .then(res => {
      this.refreshAuthorList();
    });
    
    ev.preventDefault();
    
  }
  
  componentDidMount(){
   axios.get(baseURL + "/authors")
        .then(response=>{
          this.setState({
            authors:response.data.Items
          })
        })
  }
  
  render() {


    return(
    <div>
      <div className="py-5 text-center">
        <h2>Authors</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addAuthor.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-2 mb-3">
                <label htmlFor="isbn">ID</label>
                <input type="number" className="form-control" id="aid" defaultValue="1" required />
                <div className="invalid-feedback">
                    An ID is required.
                </div>
              </div>

              <div className="col-md-4 mb-3">
                <label htmlFor="title">First Name</label>
                <input type="text" className="form-control" id="fname" defaultValue="" required />
                <div className="invalid-feedback">
                    A first name is required.
                </div>
              </div>

              <div className="col-md-4 mb-3">
                <label htmlFor="pages">Last Name</label>
                <input type="text" className="form-control" id="lname" defaultValue="" required />
                <div className="invalid-feedback">
                   A last name is required
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add Author</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
          </tr>
        </thead>
        <tbody>
            {
              this.state.authors.map((author,i)=>{
                return(<tr key={i} >
                  <td>{author.aid}</td>
                  <td>{author.fname}</td>
                  <td>{author.lname}</td>
                  <td><button className="btn btn-danger" onClick={()=>this.deleteAuthor(author.aid)}>delete</button></td>
                </tr>)
              })
            }
        </tbody>
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Authors;
