import React, { Component } from 'react';
import axios from 'axios';


const baseURL ="https://iniy5bobt6.execute-api.us-east-1.amazonaws.com/default/library";

class Books extends Component {
  constructor(props){
    super(props);
    this.state={
      books:[],
      authors:[]
    }
    this.deleteBook = this.deleteBook.bind(this);
    this.refreshBookList = this.refreshBookList.bind(this);
    this.getAuthorName = this.getAuthorName.bind(this);
  }
  getAuthorName(aid){
    var name="";
    var author = this.state.authors.filter(author=>author.aid==aid);
    name = author[0]===undefined?"Not Found":(author[0].fname + " " + author[0].lname);
    return name;
  }
   refreshBookList(){
     axios.get(baseURL + "/books")
        .then(response=>{
          this.setState({
            books:response.data.Items
          })
        })
   }
   deleteBook(isbn){
       axios.delete(baseURL+"/books",{params:{isbn}})
    .then(response=>{
          this.refreshBookList();
        })
     .catch(err=>console.log(err))
  }
  
  addBook(ev) {
    const inputISBN = ev.target.querySelector('[id="isbn"]');
    const isbn = inputISBN.value.trim();

    const inputTitle = ev.target.querySelector('[id="title"]');
    const title = inputTitle.value.trim();

    const inputPages = ev.target.querySelector('[id="pages"]');
    const pages = inputPages.value.trim();
    
    const inputAuthor = ev.target.querySelector('[id="author"]');
    const author = inputAuthor.value.trim();

    console.log( "ISBN: " + isbn );
    console.log( "Title: " + title );
    console.log( "Pages: " + pages );
    
      let newbook = {
      isbn,
      title,
      pages,
      author
    };
    
    axios.post( baseURL + "/books", newbook)
    .then(res => {
      this.refreshBookList();
    });
    
    ev.preventDefault();
    
  }
  
  componentDidMount(){
    axios.get(baseURL+"/authors")
              .then(response=>{
                this.setState({
                authors:response.data.Items
            })
       })
   axios.get(baseURL + "/books")
        .then(response=>{
          this.setState({
            books:response.data.Items
          })
       })
  }
  
  render() {


    return(
    <div>
      <div className="py-5 text-center">
        <h2>Books</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addBook.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-2 mb-3">
                <label htmlFor="isbn">ISBN</label>
                <input type="number" className="form-control" id="isbn" defaultValue="1" required />
                <div className="invalid-feedback">
                    An ISBN is required.
                </div>
              </div>

              <div className="col-md-4 mb-3">
                <label htmlFor="title">Title</label>
                <input type="text" className="form-control" id="title" defaultValue="" required />
                <div className="invalid-feedback">
                    A book title is required.
                </div>
              </div>

              <div className="col-md-2 mb-3">
                <label htmlFor="pages">Pages</label>
                <input type="number" className="form-control" id="pages" defaultValue="100" required />
                <div className="invalid-feedback">
                    The number of pages is required.
                </div>
              </div>
              
              <div className="col-md-4 mb-3">
                <label htmlFor="author">Author:</label>
                <select className="form-control" id="author" >
                  {
                    this.state.authors.map(author=><option key={author.aid} value={author.aid}>{`${author.fname} ${author.lname}`}</option>)
                  }
                </select>
                <div className="invalid-feedback">
                    The number of pages is required.
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add book</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ISBN</th>
            <th scope="col">Title</th>
            <th scope="col">Pages</th>
            <th scope="col">Author</th>
          </tr>
        </thead>
        <tbody>
            {
              this.state.books.map((book,i)=>{
                return(<tr key={i} >
                  <td>{book.isbn}</td>
                  <td>{book.title}</td>
                  <td>{book.pages}</td>
                  <td>{this.getAuthorName(book.author)}</td>
                  <td><button className="btn btn-danger" onClick={()=>this.deleteBook(book.isbn)}>delete</button></td>
                </tr>)
              })
            }
        </tbody>
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Books;
